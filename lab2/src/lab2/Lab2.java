/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

import java.util.Scanner;
/**
 *
 * @author thomas-pc
 */
public class Lab2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner s = new Scanner(System.in);
        
        int mark1;
        System.out.print("Enter Grade for CCE2050 Engineering Software Development: ");
        mark1 = Integer.parseInt(s.next());
        int mark2;
        System.out.print("Enter Grade for CCE2060 Research Methodology and Professional Project Development: ");
        mark2 = Integer.parseInt(s.next());
        int mark3;
        System.out.print("Enter Grade for CSD2550 Web Applications and Databases: ");
        mark3 = Integer.parseInt(s.next());
        int mark4;
        System.out.print("Enter Grade for PDE3413 Systems Engineering for Robotics: ");
        mark4 = Integer.parseInt(s.next());
        
        if((mark1>1 && mark1<4) && (mark2>1 && mark2<4) && (mark3>1 && mark3<4) && (mark4>1 && mark4<4))
            System.out.println("FIRST CLASS DISTINCTION Degree awarded!");
        else if((mark1>5 && mark1<8) && (mark2>5 && mark2<8) && (mark3>5 && mark3<8) && (mark4>5 && mark4<8))
            System.out.println("UPPER SECOND MERIT Degree awarded!");
        else if((mark1>9 && mark1<12) && (mark2>9 && mark2<12) && (mark3>9 && mark3<12) && (mark4>9 && mark4<12))
            System.out.println("LOWER SECOND PASS Degree awarded!");
        else if((mark1>13 && mark1<16) && (mark2>13 && mark2<16) && (mark3>13 && mark3<16) && (mark4>13 && mark4<16))
            System.out.println("THIRD PASS Degree awarded!");
        else
            System.out.println("FAIL!");
        
    }
    
}
